if [ -d src/SimpleMigrator/nupkg ]; then
    rm -r src/SimpleMigrator/nupkg
fi

dotnet tool uninstall -g SimpleMigrator
dotnet pack src/SimpleMigrator
dotnet tool install -g SimpleMigrator --add-source src/SimpleMigrator/nupkg/
