using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SimpleMigrator.Common;

public abstract class UpCommandHandler<TExecutor> where TExecutor : IDisposable
{
    protected abstract IEnumerable<string> ValidateArgs();
    protected abstract TExecutor CreateExecutor();
    protected abstract void EnsureMigrationsEnabled(TExecutor executor);
    protected abstract IEnumerable<string> GetMigrationNames(TExecutor executor);
    protected abstract void RunMigration(TExecutor executor, string migration, string migrationName);

    protected void Up(DirectoryInfo folder, string? to)
    {
        var validationErrors = ValidateArgs();
        if (validationErrors.Any())
        {
            WriteError(string.Join(Environment.NewLine, validationErrors));
            return;
        }

        using var executor = CreateExecutor();

        Console.WriteLine("Enabling migrations");
        EnsureMigrationsEnabled(executor);

        var executedScripts = GetMigrationNames(executor).ToArray(); // Convert to enumerable with known algorithms (array)
        var filesToExecute = folder!.GetFiles()
            .Select(file => (path: file.FullName, migrationName: Path.GetFileNameWithoutExtension(file.FullName)!))
            .Where(tuple => !executedScripts.Contains(tuple.migrationName) && !tuple.migrationName.EndsWith(".down"))
            .Where(tuple => string.IsNullOrWhiteSpace(to) || string.Compare(tuple.migrationName, to) <= 0)
            .OrderBy(tuple => tuple.migrationName);

        if (!string.IsNullOrWhiteSpace(to) && filesToExecute.All(tuple => tuple.migrationName != to))
        {
            WriteError($@"Migration ""{to}"" does not exist.");
            return;
        }

        if (!filesToExecute.Any())
        {
            Console.WriteLine("Nothing to do!");
            return;
        }

        foreach (var fileInfo in filesToExecute)
        {
            Console.WriteLine($"Upgrading: {fileInfo.migrationName}");
            RunMigration(executor, File.ReadAllText(fileInfo.path), fileInfo.migrationName);
        }

        Console.WriteLine("Done!");
    }

    private static void WriteError(string error)
    {
        var originalColor = Console.ForegroundColor;
        Console.ForegroundColor = ConsoleColor.Red;
        Console.Error.WriteLine(error);
        Console.ForegroundColor = originalColor;
    }
}
