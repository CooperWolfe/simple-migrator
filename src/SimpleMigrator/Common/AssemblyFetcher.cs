using System.IO;
using System.Linq;
using System.Reflection;

namespace SimpleMigrator.Common
{
    public static class AssemblyFetcher
    {
        public static string FetchContentsFromResourceName(string resourceName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var resources = assembly.GetManifestResourceNames();
            var initMigrationScriptName = resources.Single(res => res.Contains(resourceName));
            using var initMigrationScriptStream = assembly.GetManifestResourceStream(initMigrationScriptName)!;
            using var initMigrationScriptReader = new StreamReader(initMigrationScriptStream);
            return initMigrationScriptReader.ReadToEnd();
        }
    }
}