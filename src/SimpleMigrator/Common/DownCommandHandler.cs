using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SimpleMigrator.Common;

public abstract class DownCommandHandler<TExecutor> where TExecutor : IDisposable
{
    protected abstract IEnumerable<string> ValidateArgs();
    protected abstract TExecutor CreateExecutor();
    protected abstract bool MigrationsEnabled(TExecutor executor);
    protected abstract IEnumerable<string> GetExecutedScripts(TExecutor executor);
    protected abstract void RunDownwardMigration(TExecutor executor, string migration, string migrationName);

    public void Down(DirectoryInfo? folder, string? to)
    {
        var validationErrors = ValidateArgs();
        if (validationErrors.Any())
        {
            WriteError(string.Join(Environment.NewLine, validationErrors));
            return;
        }

        using var executor = CreateExecutor();
        if (!MigrationsEnabled(executor))
        {
            WriteError("Migrations are not enabled.");
            return;
        }

        var executedScripts = GetExecutedScripts(executor).ToArray(); // Enumerate once
        if (!string.IsNullOrWhiteSpace(to))
        {
            if (to.EndsWith(".down")) to = to[..^5];
            if (!executedScripts.Contains(to))
            {
                WriteError($@"Migration ""{to}"" does not exist");
                return;
            }
        }

        var filesToExecute = folder!.GetFiles()
            .Select(file => (path: file.FullName, migrationName: Path.GetFileNameWithoutExtension(file.FullName)!))
            .Where(tuple => tuple.migrationName.EndsWith(".down"))
            .Select(tuple => (path: tuple.path, migrationName: tuple.migrationName[..^5]))
            .Where(tuple => executedScripts.Contains(tuple.migrationName))
            .Where(tuple => string.IsNullOrWhiteSpace(to) || string.Compare(tuple.migrationName, to) > 0)
            .OrderByDescending(tuple => tuple.migrationName)
            .ToArray();

        if (!filesToExecute.Any())
        {
            Console.WriteLine("Nothing to do!");
            return;
        }

        foreach (var fileInfo in filesToExecute)
        {
            Console.WriteLine($"Downgrading: {fileInfo.migrationName}");
            RunDownwardMigration(executor, File.ReadAllText(fileInfo.path), fileInfo.migrationName);
        }

        Console.WriteLine("Done!");
    }

    private static void WriteError(string error)
    {
        var originalColor = Console.ForegroundColor;
        Console.ForegroundColor = ConsoleColor.Red;
        Console.Error.WriteLine(error);
        Console.ForegroundColor = originalColor;
    }
}