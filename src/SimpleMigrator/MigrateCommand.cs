using System.CommandLine;
using SimpleMigrator.Cassandra;
using SimpleMigrator.Mongo;
using SimpleMigrator.Neo4j;
using SimpleMigrator.Postgres;

namespace SimpleMigrator
{
    class MigrateCommand : RootCommand
    {
        public MigrateCommand() : base("Migrates databases")
        {
            AddCommand(new PostgresCommand());
            AddCommand(new CassandraCommand());
            AddCommand(new Neo4jCommand());
            AddCommand(new MongoCommand());
        }
    }
}