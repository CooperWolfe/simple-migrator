using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace SimpleMigrator.Mongo;

internal class DisposableMongoDatabaseAdapter : IDisposableMongoDatabase
{
    private readonly IMongoDatabase mongoDatabase;

    public DisposableMongoDatabaseAdapter(IMongoDatabase mongoDatabase)
    {
        this.mongoDatabase = mongoDatabase;
    }

    public void Dispose() { }

    public IMongoClient Client => mongoDatabase.Client;
    public DatabaseNamespace DatabaseNamespace => mongoDatabase.DatabaseNamespace;
    public MongoDatabaseSettings Settings => mongoDatabase.Settings;
    public IAsyncCursor<TResult> Aggregate<TResult>(PipelineDefinition<NoPipelineInput, TResult> pipeline, AggregateOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.Aggregate(pipeline, options, cancellationToken);
    public IAsyncCursor<TResult> Aggregate<TResult>(IClientSessionHandle session, PipelineDefinition<NoPipelineInput, TResult> pipeline, AggregateOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.Aggregate(session, pipeline, options, cancellationToken);
    public Task<IAsyncCursor<TResult>> AggregateAsync<TResult>(PipelineDefinition<NoPipelineInput, TResult> pipeline, AggregateOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.AggregateAsync(pipeline, options, cancellationToken);
    public Task<IAsyncCursor<TResult>> AggregateAsync<TResult>(IClientSessionHandle session, PipelineDefinition<NoPipelineInput, TResult> pipeline, AggregateOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.AggregateAsync(session, pipeline, options, cancellationToken);
    public void AggregateToCollection<TResult>(PipelineDefinition<NoPipelineInput, TResult> pipeline, AggregateOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.AggregateToCollection(pipeline, options, cancellationToken);
    public void AggregateToCollection<TResult>(IClientSessionHandle session, PipelineDefinition<NoPipelineInput, TResult> pipeline, AggregateOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.AggregateToCollection(session, pipeline, options, cancellationToken);
    public Task AggregateToCollectionAsync<TResult>(PipelineDefinition<NoPipelineInput, TResult> pipeline, AggregateOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.AggregateToCollectionAsync(pipeline, options, cancellationToken);
    public Task AggregateToCollectionAsync<TResult>(IClientSessionHandle session, PipelineDefinition<NoPipelineInput, TResult> pipeline, AggregateOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.AggregateToCollectionAsync(session, pipeline, options, cancellationToken);
    public void CreateCollection(string name, CreateCollectionOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.CreateCollection(name, options, cancellationToken);
    public void CreateCollection(IClientSessionHandle session, string name, CreateCollectionOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.CreateCollection(session, name, options, cancellationToken);
    public Task CreateCollectionAsync(string name, CreateCollectionOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.CreateCollectionAsync(name, options, cancellationToken);
    public Task CreateCollectionAsync(IClientSessionHandle session, string name, CreateCollectionOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.CreateCollectionAsync(session, name, options, cancellationToken);
    public void CreateView<TDocument, TResult>(string viewName, string viewOn, PipelineDefinition<TDocument, TResult> pipeline, CreateViewOptions<TDocument>? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.CreateView(viewName, viewOn, pipeline, options, cancellationToken);
    public void CreateView<TDocument, TResult>(IClientSessionHandle session, string viewName, string viewOn, PipelineDefinition<TDocument, TResult> pipeline, CreateViewOptions<TDocument>? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.CreateView(session, viewName, viewOn, pipeline, options, cancellationToken);
    public Task CreateViewAsync<TDocument, TResult>(string viewName, string viewOn, PipelineDefinition<TDocument, TResult> pipeline, CreateViewOptions<TDocument>? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.CreateViewAsync(viewName, viewOn, pipeline, options, cancellationToken);
    public Task CreateViewAsync<TDocument, TResult>(IClientSessionHandle session, string viewName, string viewOn, PipelineDefinition<TDocument, TResult> pipeline, CreateViewOptions<TDocument>? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.CreateViewAsync(session, viewName, viewOn, pipeline, options, cancellationToken);
    public void DropCollection(string name, CancellationToken cancellationToken = default)
        => mongoDatabase.DropCollection(name, cancellationToken);
    public void DropCollection(string name, DropCollectionOptions options, CancellationToken cancellationToken = default)
        => mongoDatabase.DropCollection(name, options, cancellationToken);
    public void DropCollection(IClientSessionHandle session, string name, CancellationToken cancellationToken = default)
        => mongoDatabase.DropCollection(session, name, cancellationToken);
    public void DropCollection(IClientSessionHandle session, string name, DropCollectionOptions options, CancellationToken cancellationToken = default)
        => mongoDatabase.DropCollection(session, name, cancellationToken);
    public Task DropCollectionAsync(string name, CancellationToken cancellationToken = default)
        => mongoDatabase.DropCollectionAsync(name, cancellationToken);
    public Task DropCollectionAsync(string name, DropCollectionOptions options, CancellationToken cancellationToken = default)
        => mongoDatabase.DropCollectionAsync(name, options, cancellationToken);
    public Task DropCollectionAsync(IClientSessionHandle session, string name, CancellationToken cancellationToken = default)
        => mongoDatabase.DropCollectionAsync(session, name, cancellationToken);
    public Task DropCollectionAsync(IClientSessionHandle session, string name, DropCollectionOptions options, CancellationToken cancellationToken = default)
        => mongoDatabase.DropCollectionAsync(session, name, options, cancellationToken);
    public IMongoCollection<TDocument> GetCollection<TDocument>(string name, MongoCollectionSettings? settings = null)
        => mongoDatabase.GetCollection<TDocument>(name, settings);
    public IAsyncCursor<string> ListCollectionNames(ListCollectionNamesOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.ListCollectionNames(options, cancellationToken);
    public IAsyncCursor<string> ListCollectionNames(IClientSessionHandle session, ListCollectionNamesOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.ListCollectionNames(session, options, cancellationToken);
    public Task<IAsyncCursor<string>> ListCollectionNamesAsync(ListCollectionNamesOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.ListCollectionNamesAsync(options, cancellationToken);
    public Task<IAsyncCursor<string>> ListCollectionNamesAsync(IClientSessionHandle session, ListCollectionNamesOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.ListCollectionNamesAsync(session, options, cancellationToken);
    public IAsyncCursor<BsonDocument> ListCollections(ListCollectionsOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.ListCollections(options, cancellationToken);
    public IAsyncCursor<BsonDocument> ListCollections(IClientSessionHandle session, ListCollectionsOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.ListCollections(session, options, cancellationToken);
    public Task<IAsyncCursor<BsonDocument>> ListCollectionsAsync(ListCollectionsOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.ListCollectionsAsync(options, cancellationToken);
    public Task<IAsyncCursor<BsonDocument>> ListCollectionsAsync(IClientSessionHandle session, ListCollectionsOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.ListCollectionsAsync(session, options, cancellationToken);
    public void RenameCollection(string oldName, string newName, RenameCollectionOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.RenameCollection(oldName, newName, options, cancellationToken);
    public void RenameCollection(IClientSessionHandle session, string oldName, string newName, RenameCollectionOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.RenameCollection(session, oldName, newName, options, cancellationToken);
    public Task RenameCollectionAsync(string oldName, string newName, RenameCollectionOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.RenameCollectionAsync(oldName, newName, options, cancellationToken);
    public Task RenameCollectionAsync(IClientSessionHandle session, string oldName, string newName, RenameCollectionOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.RenameCollectionAsync(session, oldName, newName, options, cancellationToken);
    public TResult RunCommand<TResult>(Command<TResult> command, ReadPreference? readPreference = null, CancellationToken cancellationToken = default)
        => mongoDatabase.RunCommand(command, readPreference, cancellationToken);
    public TResult RunCommand<TResult>(IClientSessionHandle session, Command<TResult> command, ReadPreference? readPreference = null, CancellationToken cancellationToken = default)
        => mongoDatabase.RunCommand(session, command, readPreference, cancellationToken);
    public Task<TResult> RunCommandAsync<TResult>(Command<TResult> command, ReadPreference? readPreference = null, CancellationToken cancellationToken = default)
        => mongoDatabase.RunCommandAsync(command, readPreference, cancellationToken);
    public Task<TResult> RunCommandAsync<TResult>(IClientSessionHandle session, Command<TResult> command, ReadPreference? readPreference = null, CancellationToken cancellationToken = default)
        => mongoDatabase.RunCommandAsync(session, command, readPreference, cancellationToken);
    public IChangeStreamCursor<TResult> Watch<TResult>(PipelineDefinition<ChangeStreamDocument<BsonDocument>, TResult> pipeline, ChangeStreamOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.Watch(pipeline, options, cancellationToken);
    public IChangeStreamCursor<TResult> Watch<TResult>(IClientSessionHandle session, PipelineDefinition<ChangeStreamDocument<BsonDocument>, TResult> pipeline, ChangeStreamOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.Watch(session, pipeline, options, cancellationToken);
    public Task<IChangeStreamCursor<TResult>> WatchAsync<TResult>(PipelineDefinition<ChangeStreamDocument<BsonDocument>, TResult> pipeline, ChangeStreamOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.WatchAsync(pipeline, options, cancellationToken);
    public Task<IChangeStreamCursor<TResult>> WatchAsync<TResult>(IClientSessionHandle session, PipelineDefinition<ChangeStreamDocument<BsonDocument>, TResult> pipeline, ChangeStreamOptions? options = null, CancellationToken cancellationToken = default)
        => mongoDatabase.WatchAsync(session, pipeline, options, cancellationToken);
    public IMongoDatabase WithReadConcern(ReadConcern readConcern)
        => mongoDatabase.WithReadConcern(readConcern);
    public IMongoDatabase WithReadPreference(ReadPreference readPreference)
        => mongoDatabase.WithReadPreference(readPreference);
    public IMongoDatabase WithWriteConcern(WriteConcern writeConcern)
        => mongoDatabase.WithWriteConcern(writeConcern);
}