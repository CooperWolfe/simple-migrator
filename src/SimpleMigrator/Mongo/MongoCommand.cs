using System.CommandLine;
using SimpleMigrator.Mongo.Down;
using SimpleMigrator.Mongo.Up;

namespace SimpleMigrator.Mongo
{
    class MongoCommand : Command
    {
        public MongoCommand() : base("mongo", "Migrates a Mongo database")
        {
            AddCommand(new UpCommand());
            AddCommand(new DownCommand());
        }
    }
}