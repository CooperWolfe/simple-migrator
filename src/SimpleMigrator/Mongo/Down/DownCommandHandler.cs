using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace SimpleMigrator.Mongo.Down;

class DownCommandHandler : Common.DownCommandHandler<IDisposableMongoDatabase>
{
    private const string MigrationScriptCollection = "_migrations";

    private string? connectionString;
    private string? database;
    private DirectoryInfo? folder;

    public void Down(string? connectionString, string? database, DirectoryInfo? folder, string? to)
    {
        this.connectionString = connectionString;
        this.database = database;
        this.folder = folder;
        base.Down(folder, to);
    }

    protected override IEnumerable<string> ValidateArgs()
    {
        if (string.IsNullOrWhiteSpace(connectionString))
        {
            yield return $"{DownCommand.ConnectionStringOptionName} is required.";
        }
        if (string.IsNullOrWhiteSpace(database))
        {
            yield return $"{DownCommand.DatabaseOptionName} is required.";
        }
        if (folder is null)
        {
            yield return $"{DownCommand.FolderOptionName} is required.";
        }
        else if (!folder.Exists)
        {
            yield return $"Folder {folder.FullName} does not exist.";
        }
    }

    protected override IDisposableMongoDatabase CreateExecutor()
    {
        var client = new MongoClient(connectionString);
        var mongoDatabase = client.GetDatabase(database);
        return new DisposableMongoDatabaseAdapter(mongoDatabase);
    }

    protected override IEnumerable<string> GetExecutedScripts(IDisposableMongoDatabase database)
    {
        var collection = database.GetCollection<MigrationScript>(MigrationScriptCollection);
        var scripts = collection.Find(_ => true)
            .ToList()
            .OrderBy(script => script.Name)
            .Select(script => script.Name);
        return scripts;
    }

    protected override bool MigrationsEnabled(IDisposableMongoDatabase database)
    {
        var collection = database.GetCollection<MigrationScript>(MigrationScriptCollection);
        long numScripts = collection.EstimatedDocumentCount();
        return numScripts > 0;
    }

    protected override void RunDownwardMigration(IDisposableMongoDatabase database, string migration, string migrationName)
    {
        var migrationCommand = BsonDocument.Parse(migration);
        database.RunCommand<BsonDocument>(migrationCommand);

        var collection = database.GetCollection<MigrationScript>(MigrationScriptCollection);
        collection.DeleteOne(script => script.Name == migrationName);
    }

    private class MigrationScript
    {
        [BsonId]
        [BsonElement(elementName: "name")]
        public string Name { get; set; } = string.Empty;

        [BsonElement(elementName: "createdAt")]
        public DateTime CreatedAt { get; set; }
    }
}