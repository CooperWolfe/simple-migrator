using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;

namespace SimpleMigrator.Mongo.Down
{
    class DownCommand : Command
    {
        public const string ConnectionStringOptionName = "--connection-string";
        public const string DatabaseOptionName = "--database";
        public const string FolderOptionName = "--folder";
        public const string ToOptionName = "--to";
        
        public DownCommand() : base("down", "Migrates the Mongo database backward")
        {
            var connectionStringOption = new Option<string?>(
                ConnectionStringOptionName,
                description: "The connection string of the Mongo host",
                arity: ArgumentArity.ExactlyOne);
            connectionStringOption.AddAlias("-c");
            AddOption(connectionStringOption);

            var databaseOption = new Option<string?>(
                DatabaseOptionName,
                description: "The Mongo database to connect to",
                arity: ArgumentArity.ExactlyOne);
            databaseOption.AddAlias("-d");
            AddOption(databaseOption);

            var folderOption = new Option<DirectoryInfo?>(
                FolderOptionName,
                description: "The folder within which the SQL scripts to run live",
                arity: ArgumentArity.ExactlyOne);
            folderOption.AddAlias("-f");
            AddOption(folderOption);

            var toOption = new Option<string?>(
                ToOptionName,
                description: "The down migration to migrate down to",
                arity: ArgumentArity.ZeroOrOne);
            AddOption(toOption);

            var handler = new DownCommandHandler();
            Handler = CommandHandler.Create<string?, string?, DirectoryInfo?, string?>(handler.Down);
        }
    }
}