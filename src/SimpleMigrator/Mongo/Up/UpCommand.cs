using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;

namespace SimpleMigrator.Mongo.Up;
class UpCommand : Command
{
    public const string ConnectionStringOptionName = "--connection-string";
    public const string DatabaseOptionName = "--database";
    public const string FolderOptionName = "--folder";
    public const string ToOptionName = "--to";

    public UpCommand() : base("up", "Migrates the Mongo database forward")
    {
        var connectionStringOption = new Option<string?>(
            ConnectionStringOptionName,
            description: "The connection string of the Mongo server",
            arity: ArgumentArity.ExactlyOne);
        connectionStringOption.AddAlias("-c");
        AddOption(connectionStringOption);

        var databaseOption = new Option<string?>(
            DatabaseOptionName,
            description: "Which Mongo database to connect to",
            arity: ArgumentArity.ExactlyOne);
        databaseOption.AddAlias("-d");
        AddOption(databaseOption);

        var folderOption = new Option<DirectoryInfo?>(
            FolderOptionName,
            description: "The folder within which the migrations to run live",
            arity: ArgumentArity.ExactlyOne);
        folderOption.AddAlias("-f");
        AddOption(folderOption);

        var toOption = new Option<string?>(
            ToOptionName,
            description: "The migration to migrate the database up to",
            arity: ArgumentArity.ZeroOrOne);
        AddOption(toOption);

        var handler = new UpCommandHandler();
        Handler = CommandHandler.Create<string?, string?, DirectoryInfo?, string?>(handler.Up);
    }
}
