using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace SimpleMigrator.Mongo.Up;

class UpCommandHandler : Common.UpCommandHandler<IDisposableMongoDatabase>
{
    private const string InitScriptName = "20220620-01-init-migration-scripts";
    private const string MigrationScriptCollection = "_migrations";

    private string? connectionString;
    private string? database;
    private DirectoryInfo? folder;

    public void Up(string? connectionString, string? database, DirectoryInfo? folder, string? to)
    {
        this.connectionString = connectionString;
        this.database = database;
        this.folder = folder;
        base.Up(folder!, to);
    }

    protected override IEnumerable<string> ValidateArgs()
    {
        if (string.IsNullOrWhiteSpace(connectionString))
        {
            yield return $"{UpCommand.ConnectionStringOptionName} is required.";
        }
        if (string.IsNullOrWhiteSpace(database))
        {
            yield return $"{UpCommand.DatabaseOptionName} is required.";
        }
        if (folder is null)
        {
            yield return $"{UpCommand.FolderOptionName} is required.";
        }
        else if (!folder.Exists)
        {
            yield return $"Folder {folder.FullName} does not exist.";
        }
    }

    protected override IDisposableMongoDatabase CreateExecutor()
    {
        var client = new MongoClient(connectionString);
        var mongoDatabase = client.GetDatabase(database);
        return new DisposableMongoDatabaseAdapter(mongoDatabase);
    }

    protected override void EnsureMigrationsEnabled(IDisposableMongoDatabase database)
    {
        var collection = database.GetCollection<MigrationScript>(MigrationScriptCollection);
        var initialScript = collection.Find(script => script.Name == InitScriptName).SingleOrDefault();
        if (initialScript != null) return;

        initialScript = new MigrationScript
        {
            Name = InitScriptName,
            CreatedAt = DateTime.UtcNow
        };
        collection.InsertOne(initialScript);
    }

    protected override IEnumerable<string> GetMigrationNames(IDisposableMongoDatabase database)
    {
        var collection = database.GetCollection<MigrationScript>(MigrationScriptCollection);
        var scriptNames = collection.Find(_ => true)
            .ToList()
            .OrderBy(script => script.Name)
            .Select(script => script.Name);
        return scriptNames;
    }

    protected override void RunMigration(IDisposableMongoDatabase database, string migration, string migrationName)
    {
        var migrationCommand = BsonDocument.Parse(migration);
        database.RunCommand<BsonDocument>(migrationCommand);

        var collection = database.GetCollection<MigrationScript>(MigrationScriptCollection);
        var migrationScript = new MigrationScript
        {
            Name = migrationName,
            CreatedAt = DateTime.UtcNow
        };
        collection.InsertOne(migrationScript);
    }

    private class MigrationScript
    {
        [BsonId]
        [BsonElement(elementName: "name")]
        public string Name { get; set; } = string.Empty;

        [BsonElement(elementName: "createdAt")]
        public DateTime CreatedAt { get; set; }
    }
}
