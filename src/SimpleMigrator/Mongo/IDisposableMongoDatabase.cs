using System;
using MongoDB.Driver;

namespace SimpleMigrator.Mongo;
internal interface IDisposableMongoDatabase : IMongoDatabase, IDisposable { }
