using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Dapper;
using Npgsql;
using SimpleMigrator.Common;

namespace SimpleMigrator.Postgres.Up;

public class UpCommandHandler : Common.UpCommandHandler<NpgsqlConnection>
{
    private const string InitScriptName = "20210624-01-init-migration-scripts";
    private const int MigrationScriptTimeoutSeconds = 600; // 10m

    private string? connectionString;
    private DirectoryInfo? folder;
    private string[] variables = Array.Empty<string>();
    private readonly Dictionary<string, object> variableParameters = new();

    public void Up(string? connectionString, DirectoryInfo? folder, string? to, string[] var)
    {
        this.connectionString = connectionString;
        this.folder = folder;
        this.variables = var;
        base.Up(folder!, to);
    }

    protected override IEnumerable<string> ValidateArgs()
    {
        if (string.IsNullOrWhiteSpace(connectionString))
        {
            yield return $"{UpCommand.ConnectionStringOptionName} is required.";
        }
        if (folder is null)
        {
            yield return $"{UpCommand.FolderOptionName} is required.";
        }
        else if (!folder.Exists)
        {
            yield return $"Folder {folder.FullName} does not exist.";
        }
        foreach (var variable in variables)
        {
            var tokens = variable.Split('=');
            if (tokens.Length != 2)
            {
                yield return $"Variable {variable} is not in the format key=value.";
            }
            if (!variableParameters.TryAdd(tokens[0], tokens[1]))
            {
                yield return $"Variable {tokens[0]} is duplicated.";
            }
        }
    }

    protected override NpgsqlConnection CreateExecutor()
    {
        return new NpgsqlConnection(connectionString!);
    }

    protected override void EnsureMigrationsEnabled(NpgsqlConnection connection)
    {
        bool tableExists = connection.ExecuteScalar<int>(@"SELECT count(1) FROM information_schema.tables WHERE table_schema = 'migration' AND table_name = 'script'") > 0;
        if (tableExists)
        {
            return;
        }

        string migration = AssemblyFetcher.FetchContentsFromResourceName(InitScriptName);
        connection.Execute(migration);
        connection.Execute(
            @"INSERT INTO migration.script (name, created_at) VALUES (@name, NOW() at time zone 'utc') on conflict (name) do nothing",
            new { name = InitScriptName });
    }

    protected override IEnumerable<string> GetMigrationNames(NpgsqlConnection connection)
    {
        return connection.Query<string>("SELECT name FROM migration.script");
    }

    protected override void RunMigration(NpgsqlConnection connection, string migration, string migrationName)
    {
        migration = Regex.Replace(migration, @"\$\$([^\$]+)\$\$", match => variableParameters[match.Groups[1].Value].ToString()!);
        connection.Execute(
            migration,
            new DynamicParameters(variableParameters),
            commandTimeout: MigrationScriptTimeoutSeconds);
        connection.Execute(
            @"INSERT INTO migration.script (name, created_at) VALUES (@name, NOW() at time zone 'utc')",
            new { name = migrationName });
    }
}
