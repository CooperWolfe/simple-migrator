using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Dapper;
using Npgsql;

namespace SimpleMigrator.Postgres.Down;

public class DownCommandHandler : Common.DownCommandHandler<NpgsqlConnection>
{
    private const int MigrationScriptTimeoutSeconds = 600; // 10m

    private string? connectionString;
    private DirectoryInfo? folder;
    private string[] variables = Array.Empty<string>();
    private readonly Dictionary<string, object> variableParameters = new();

    public void Down(string? connectionString, DirectoryInfo? folder, string? to, string[] var)
    {
        this.connectionString = connectionString;
        this.folder = folder;
        this.variables = var;
        base.Down(folder, to);
    }

    protected override IEnumerable<string> ValidateArgs()
    {
        if (string.IsNullOrWhiteSpace(connectionString))
        {
            yield return $"{DownCommand.ConnectionStringOptionName} is required.";
        }
        if (folder is null)
        {
            yield return $"{DownCommand.FolderOptionName} is required.";
        }
        else if (!folder.Exists)
        {
            yield return $"Folder {folder.FullName} does not exist.";
        }
        foreach (var variable in variables)
        {
            var tokens = variable.Split('=');
            if (tokens.Length != 2)
            {
                yield return $"Variable {variable} is not in the format key=value.";
            }
            if (!variableParameters.TryAdd(tokens[0], tokens[1]))
            {
                yield return $"Variable {tokens[0]} is duplicated.";
            }
        }
    }

    protected override NpgsqlConnection CreateExecutor()
    {
        return new NpgsqlConnection(connectionString!);
    }

    protected override IEnumerable<string> GetExecutedScripts(NpgsqlConnection connection)
    {
        return connection.Query<string>("SELECT name FROM migration.script");
    }

    protected override bool MigrationsEnabled(NpgsqlConnection connection)
    {
        return connection.ExecuteScalar<int>(@"SELECT count(1) FROM information_schema.tables WHERE table_schema = 'migration' AND table_name = 'script'") > 0;
    }

    protected override void RunDownwardMigration(NpgsqlConnection connection, string migration, string migrationName)
    {
        migration = Regex.Replace(migration, @"\$\$([^\$]+)\$\$", match => variableParameters[match.Groups[1].Value].ToString()!);
        connection.Execute(
            migration,
            new DynamicParameters(variableParameters),
            commandTimeout: MigrationScriptTimeoutSeconds);
        connection.Execute(
            @"DELETE FROM migration.script WHERE name >= @name",
            new { name = migrationName });
    }
}