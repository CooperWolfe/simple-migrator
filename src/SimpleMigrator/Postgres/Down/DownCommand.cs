using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.Data;
using System.IO;
using System.Linq;
using Dapper;
using Npgsql;

namespace SimpleMigrator.Postgres.Down
{
    class DownCommand : Command
    {
        public const string ConnectionStringOptionName = "--connection-string";
        public const string FolderOptionName = "--folder";
        public const string ToOptionName = "--to";
        public const string VariableOptionName = "--var";
        
        public DownCommand() : base("down", "Migrates the postgres database backward")
        {
            var connectionStringOption = new Option<string?>(
                ConnectionStringOptionName,
                description: "The connection string to the Postgres database",
                arity: ArgumentArity.ExactlyOne);
            connectionStringOption.AddAlias("-c");
            AddOption(connectionStringOption);

            var folderOption = new Option<DirectoryInfo?>(
                FolderOptionName,
                description: "The folder within which the SQL scripts to run live",
                arity: ArgumentArity.ExactlyOne);
            folderOption.AddAlias("-f");
            AddOption(folderOption);

            var toOption = new Option<string?>(
                ToOptionName,
                description: "The down migration to migrate down to",
                arity: ArgumentArity.ZeroOrOne);
            AddOption(toOption);

            var varOption = new Option<string[]>(
                VariableOptionName,
                description: "A variable to pass to the SQL scripts",
                arity: ArgumentArity.ZeroOrMore)
            {
                AllowMultipleArgumentsPerToken = true
            };
            varOption.AddAlias("-v");
            AddOption(varOption);

            var handler = new DownCommandHandler();
            Handler = CommandHandler.Create<string, DirectoryInfo, string?, string[]>(handler.Down);
        }
    }
}