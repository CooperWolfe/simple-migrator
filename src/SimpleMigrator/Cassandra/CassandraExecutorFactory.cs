using Cassandra;

namespace SimpleMigrator.Cassandra;

public static class CassandraExecutorFactory
{
    public static CassandraExecutor Create(string host, int port, string keyspace)
    {
        var cluster = Cluster.Builder()
            .AddContactPoints(host!)
            .WithPort(port)
            .Build();
        var session = cluster.Connect(keyspace);
        return new CassandraExecutor(cluster, session);
    }
}