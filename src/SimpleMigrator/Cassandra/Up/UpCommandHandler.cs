using System.Collections.Generic;
using System.IO;
using System.Linq;
using SimpleMigrator.Common;

namespace SimpleMigrator.Cassandra.Up;

public class UpCommandHandler : Common.UpCommandHandler<CassandraExecutor>
{
    private const string InitDdlScriptName = "20220307-01-init-migration-scripts";
    private const string InitDataScriptName = "20220313-01-init-populate-initial-migrations";

    private string? host;
    private int? port;
    private string? keyspace;
    private DirectoryInfo? folder;

    public void Up(string? host, int? port, string? keyspace, DirectoryInfo? folder, string? to)
    {
        this.host = host;
        this.port = port;
        this.keyspace = keyspace;
        this.folder = folder;
        base.Up(folder!, to);
    }

    protected override IEnumerable<string> ValidateArgs()
    {
        if (string.IsNullOrWhiteSpace(host))
        {
            yield return $"{UpCommand.HostOptionName} is required.";
        }
        if (port is null)
        {
            yield return $"{UpCommand.PortOptionName} is required.";
        }
        if (string.IsNullOrWhiteSpace(keyspace))
        {
            yield return $"{UpCommand.KeyspaceOptionName}";
        }
        if (folder is null)
        {
            yield return $"{UpCommand.FolderOptionName} is required.";
        }
        else if (!folder.Exists)
        {
            yield return $"Folder {folder.FullName} does not exist.";
        }
    }

    protected override CassandraExecutor CreateExecutor()
    {
        return CassandraExecutorFactory.Create(host!, port!.Value, keyspace!);
    }

    protected override void EnsureMigrationsEnabled(CassandraExecutor executor)
    {
        bool migrationScriptTableDoesNotExist;
        using (var session = executor.Cluster.Connect("system_schema"))
        {
            var rows = session.Execute(@$"
                select count(table_name) as count from tables
                where keyspace_name='{executor.Session.Keyspace}'
                  and table_name = 'migration_script';");
            migrationScriptTableDoesNotExist = rows.Single().GetValue<long>("count") == 0;
        }
        if (migrationScriptTableDoesNotExist)
        {
            string ddlMigration = AssemblyFetcher.FetchContentsFromResourceName(InitDdlScriptName);
            executor.Session.Execute(ddlMigration);

            string dataMigration = AssemblyFetcher.FetchContentsFromResourceName(InitDataScriptName);
            executor.Session.Execute(dataMigration);
        }
    }

    protected override IEnumerable<string> GetMigrationNames(CassandraExecutor executor)
    {
        return executor.Session
            .Execute("select name from migration_script")
            .Select(row => row.GetValue<string>("name"));
    }

    protected override void RunMigration(CassandraExecutor executor, string migration, string migrationName)
    {
        executor.Session.Execute(migration);
        var trackingStatement = executor.Session.Prepare("insert into migration_script (name, created_at) values (?, toTimestamp(now()))");
        executor.Session.Execute(trackingStatement.Bind(migrationName));
    }
}