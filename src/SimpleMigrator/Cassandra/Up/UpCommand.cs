using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.IO;

namespace SimpleMigrator.Cassandra.Up;

public class UpCommand : Command
{
    public const string HostOptionName = "--host";
    public const string PortOptionName = "--port";
    public const string KeyspaceOptionName = "--keyspace";
    public const string FolderOptionName = "--folder";
    public const string ToOptionName = "--to";
    private const string InitScriptName = "20220307-01-init-migration-scripts";

    public UpCommand() : base("up", "Migrates the cassandra database forward")
    {
        var hostOption = new Option<string?>(
            HostOptionName,
            description: "The host of the Cassandra cluster",
            arity: ArgumentArity.ExactlyOne);
        hostOption.AddAlias("-h");
        AddOption(hostOption);

        var portOption = new Option<int?>(
            PortOptionName,
            description: "The port of the cassandra cluster",
            arity: ArgumentArity.ExactlyOne);
        portOption.AddAlias("-p");
        AddOption(portOption);

        var keyspaceOption = new Option<string?>(
            KeyspaceOptionName,
            description: "The keyspace to migrate",
            arity: ArgumentArity.ExactlyOne);
        keyspaceOption.AddAlias("-k");
        AddOption(keyspaceOption);

        var folderOption = new Option<DirectoryInfo?>(
            FolderOptionName,
            description: "The folder within which the SQL scripts to run live",
            arity: ArgumentArity.ExactlyOne);
        folderOption.AddAlias("-f");
        AddOption(folderOption);

        var toOption = new Option<string?>(
            ToOptionName,
            description: "The migration to migrate the database up to",
            arity: ArgumentArity.ZeroOrOne);
        AddOption(toOption);

        var handler = new UpCommandHandler();
        Handler = CommandHandler.Create<string?, int?, string?, DirectoryInfo?, string?>(handler.Up);
    }
}