using System.CommandLine;
using SimpleMigrator.Cassandra.Down;
using SimpleMigrator.Cassandra.Up;

namespace SimpleMigrator.Cassandra;

public class CassandraCommand : Command
{
    public CassandraCommand() : base("cassandra", "Migrates a cassandra database")
    {
        AddCommand(new UpCommand());
        AddCommand(new DownCommand());
    }
}