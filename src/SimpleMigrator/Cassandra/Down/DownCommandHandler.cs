using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SimpleMigrator.Cassandra.Down;

public class DownCommandHandler : Common.DownCommandHandler<CassandraExecutor>
{
    private string? host;
    private int? port;
    private string? keyspace;
    private DirectoryInfo? folder;

    public void Down(string? host, int? port, string? keyspace, DirectoryInfo? folder, string? to)
    {
        this.host = host;
        this.port = port;
        this.keyspace = keyspace;
        this.folder = folder;
        base.Down(folder, to);
    }

    protected override IEnumerable<string> ValidateArgs()
    {
        if (string.IsNullOrWhiteSpace(host))
        {
            yield return $"{DownCommand.HostOptionName} is required.";
        }
        if (port is null)
        {
            yield return $"{DownCommand.PortOptionName} is required.";
        }
        if (string.IsNullOrWhiteSpace(keyspace))
        {
            yield return $"{DownCommand.KeyspaceOptionName} is required.";
        }
        if (folder is null)
        {
            yield return $"{DownCommand.FolderOptionName} is required.";
        }
        else if (!folder.Exists)
        {
            yield return $"Folder {folder.Name} does not exist.";
        }
    }

    protected override CassandraExecutor CreateExecutor()
    {
        return CassandraExecutorFactory.Create(host!, port!.Value, keyspace!);
    }

    protected override IEnumerable<string> GetExecutedScripts(CassandraExecutor executor)
    {
        return executor.Session
            .Execute("select name from migration_script")
            .Select(row => row.GetValue<string>("name"));
    }

    protected override bool MigrationsEnabled(CassandraExecutor executor)
    {
        using var session = executor.Cluster.Connect("system_schema");
        var result = session.Execute($"select count(table_name) as count from tables where keyspace_name='{executor.Session.Keyspace}';");
        return result.SingleOrDefault()?.GetValue<long>("count") > 0;
    }

    protected override void RunDownwardMigration(CassandraExecutor executor, string migration, string migrationName)
    {
        executor.Session.Execute(migration);
        var deleteStatement = executor.Session.Prepare(@"delete from migration_script where name = ?");
        executor.Session.Execute(deleteStatement.Bind(migrationName));
    }
}