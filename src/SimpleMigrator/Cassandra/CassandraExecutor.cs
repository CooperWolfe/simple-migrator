using System;
using Cassandra;

namespace SimpleMigrator.Cassandra;

public class CassandraExecutor : IDisposable
{
    public CassandraExecutor(Cluster cluster, ISession session)
    {
        Cluster = cluster;
        Session = session;
    }

    public Cluster Cluster { get; }
    public ISession Session { get; }

    public void Dispose()
    {
        Session.Dispose();
        Cluster.Dispose();
    }
}