using System.Collections.Generic;
using System.IO;
using Neo4j.Driver;
using Neo4j.Driver.Extensions;

namespace SimpleMigrator.Neo4j.Down;

class DownCommandHandler : Common.DownCommandHandler<IAsyncSession>
{
    private string? host;
    private int? port;
    private string? database;
    private string? username;
    private string? password;
    private DirectoryInfo? folder;

    public void Down(string? host, int? port, string? database, string? username, string? password, DirectoryInfo? folder, string? to)
    {
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
        this.folder = folder;
        base.Down(folder, to);
    }

    protected override IEnumerable<string> ValidateArgs()
    {
        if (string.IsNullOrWhiteSpace(host))
        {
            yield return $"{DownCommand.HostOptionName} is required.";
        }
        if (!port.HasValue)
        {
            yield return $"{DownCommand.PortOptionName} is required.";
        }
        if (string.IsNullOrWhiteSpace(database))
        {
            yield return $"{DownCommand.DatabaseOptionName} is required.";
        }
        if (string.IsNullOrWhiteSpace(username))
        {
            yield return $"{DownCommand.UsernameOptionName} is required.";
        }
        if (string.IsNullOrWhiteSpace(password))
        {
            yield return $"{DownCommand.PasswordOptionName} is required.";
        }
        if (folder is null)
        {
            yield return $"{DownCommand.FolderOptionName} is required.";
        }
        else if (!folder.Exists)
        {
            yield return $"Folder {folder.FullName} does not exist.";
        }
    }

    protected override IAsyncSession CreateExecutor()
    {
        string uri = $"bolt://{host}:{port}";
        var driver = GraphDatabase.Driver(uri, AuthTokens.Basic(username, password));
        return driver.AsyncSession(cfg => cfg.WithDatabase(database));
    }

    protected override IEnumerable<string> GetExecutedScripts(IAsyncSession session)
    {
        var query = new Query(@"
            MATCH (m:Migration)
            RETURN m
            ORDER BY m.name");
        var cursor = session.RunAsync(query).Result;
        var results = cursor.ToListAsync(record =>
        {
            var migrationNode = record["m"].As<INode>();
            return migrationNode.GetValueStrict<string>("name");
        }).Result;
        return results;
    }

    protected override bool MigrationsEnabled(IAsyncSession session)
    {
        var query = new Query("MATCH (m:Migration) RETURN m");
        var cursor = session.RunAsync(query).Result;
        bool wereResults = cursor.FetchAsync().Result;
        return wereResults;
    }

    protected override void RunDownwardMigration(IAsyncSession session, string migration, string migrationName)
    {
        var migrationQuery = new Query(migration);
        session.RunAsync(migrationQuery).Wait();

        var trackingQuery = new Query(@"
            MATCH (m:Migration { name: $migrationName })
            DELETE m",
            new { migrationName });
        session.RunAsync(trackingQuery).Wait();
    }
}