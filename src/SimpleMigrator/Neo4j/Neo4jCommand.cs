using System.CommandLine;
using SimpleMigrator.Neo4j.Down;
using SimpleMigrator.Neo4j.Up;

namespace SimpleMigrator.Neo4j
{
    class Neo4jCommand : Command
    {
        public Neo4jCommand() : base("neo4j", "Migrates a Neo4j database")
        {
            AddCommand(new UpCommand());
            AddCommand(new DownCommand());
        }
    }
}