using System;
using System.Collections.Generic;
using System.IO;
using Neo4j.Driver;
using Neo4j.Driver.Extensions;

namespace SimpleMigrator.Neo4j.Up;

class UpCommandHandler : Common.UpCommandHandler<IAsyncSession>
{
    private const string InitScriptName = "20220526-01-init-migration-scripts";

    private string? host;
    private int? port;
    private string? database;
    private string? username;
    private string? password;
    private DirectoryInfo? folder;

    public void Up(string? host, int? port, string? database, string? username, string? password, DirectoryInfo? folder, string? to)
    {
        this.host = host;
        this.port = port;
        this.database = database;
        this.username = username;
        this.password = password;
        this.folder = folder;
        base.Up(folder!, to);
    }

    protected override IEnumerable<string> ValidateArgs()
    {
        if (string.IsNullOrWhiteSpace(host))
        {
            yield return $"{UpCommand.HostOptionName} is required.";
        }
        if (!port.HasValue)
        {
            yield return $"{UpCommand.PortOptionName} is required.";
        }
        if (string.IsNullOrWhiteSpace(database))
        {
            yield return $"{UpCommand.DatabaseOptionName} is required.";
        }
        if (string.IsNullOrWhiteSpace(username))
        {
            yield return $"{UpCommand.UsernameOptionName} is required.";
        }
        if (string.IsNullOrWhiteSpace(password))
        {
            yield return $"{UpCommand.PasswordOptionName} is required.";
        }
        if (folder is null)
        {
            yield return $"{UpCommand.FolderOptionName} is required.";
        }
        else if (!folder.Exists)
        {
            yield return $"Folder {folder.FullName} does not exist.";
        }
    }

    protected override IAsyncSession CreateExecutor()
    {
        string uri = $"bolt://{host}:{port}";
        var driver = GraphDatabase.Driver(uri, AuthTokens.Basic(username, password));
        return driver.AsyncSession(cfg => cfg.WithDatabase(database));
    }

    protected override void EnsureMigrationsEnabled(IAsyncSession session)
    {
        var query = new Query(@"
            MERGE (m:Migration { name: $InitScriptName })
            ON CREATE SET m.createdAt = $Now
            RETURN m",
            new
            {
                InitScriptName,
                Now = new ZonedDateTime(DateTimeOffset.UtcNow)
            });
        session.RunAsync(query).Wait();
    }

    protected override IEnumerable<string> GetMigrationNames(IAsyncSession session)
    {
        var query = new Query(@"
            MATCH (m:Migration)
            RETURN m
            ORDER BY m.name");
        var cursor = session.RunAsync(query).Result;
        var results = cursor.ToListAsync(record =>
        {
            var migrationNode = record["m"].As<INode>();
            return migrationNode.GetValueStrict<string>("name");
        }).Result;
        return results;
    }

    protected override void RunMigration(IAsyncSession session, string migration, string migrationName)
    {
        var migrationQuery = new Query(migration);
        session.RunAsync(migrationQuery).Wait();

        var trackingQuery = new Query(@$"
            CREATE (m:Migration {{ name: $migrationName, createdAt: $now }})",
            new
            {
                migrationName,
                now = new ZonedDateTime(DateTimeOffset.UtcNow)
            });
        session.RunAsync(trackingQuery).Wait();
    }
}
