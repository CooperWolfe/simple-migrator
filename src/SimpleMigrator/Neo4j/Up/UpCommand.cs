using System.CommandLine;
using System.CommandLine.Invocation;
using System.IO;

namespace SimpleMigrator.Neo4j.Up
{
    class UpCommand : Command
    {
        public const string HostOptionName = "--host";
        public const string PortOptionName = "--port";
        public const string DatabaseOptionName = "--database";
        public const string UsernameOptionName = "--username";
        public const string PasswordOptionName = "--password";
        public const string FolderOptionName = "--folder";
        public const string ToOptionName = "--to";
        
        public UpCommand() : base("up", "Migrates the neo4j database forward")
        {
            var hostOption = new Option<string?>(
                HostOptionName,
                description: "The host of the Neo4j database",
                arity: ArgumentArity.ExactlyOne);
            hostOption.AddAlias("-h");
            AddOption(hostOption);

            var portOption = new Option<int?>(
                PortOptionName,
                description: "The port of the Neo4j database",
                arity: ArgumentArity.ExactlyOne);
            portOption.AddAlias("-p");
            AddOption(portOption);

            var databaseOption = new Option<string?>(
                DatabaseOptionName,
                description: "The Neo4j database to connect to",
                arity: ArgumentArity.ExactlyOne);
            databaseOption.AddAlias("-d");
            AddOption(databaseOption);

            var usernameOption = new Option<string?>(
                UsernameOptionName,
                description: "The username with which to connect to the Neo4j database",
                arity: ArgumentArity.ExactlyOne);
            usernameOption.AddAlias("-u");
            AddOption(usernameOption);

            var passwordOption = new Option<string?>(
                PasswordOptionName,
                description: "The password to use when connecting to the Neo4j database",
                arity: ArgumentArity.ExactlyOne);
            passwordOption.AddAlias("-w");
            AddOption(passwordOption);

            var folderOption = new Option<DirectoryInfo?>(
                FolderOptionName,
                description: "The folder within which the Cypher scripts to run live",
                arity: ArgumentArity.ExactlyOne);
            folderOption.AddAlias("-f");
            AddOption(folderOption);

            var toOption = new Option<string?>(
                ToOptionName,
                description: "The migration to migrate the database up to",
                arity: ArgumentArity.ZeroOrOne);
            AddOption(toOption);

            var handler = new UpCommandHandler();
            Handler = CommandHandler.Create<string?, int?, string?, string?, string?, DirectoryInfo?, string?>(handler.Up);
        }
    }
}