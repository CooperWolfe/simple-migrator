# Simple Migrator

- [Installation](#installation)
- [Databases Supported](#databases-supported)
- [Usage](#usage)
  - [Postgres](#postgres)
  - [Cassandra](#cassandra)
  - [Neo4j](#neo4j)
  - [Mongo](#mongo)
- [Contribution](#contribution)

## Installation
This is intended to be installed as a [dotnet tool](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-tool-install). The [install.sh](install.sh) script helps with this locally. Otherwise, use `dotnet tool install [-g] SimpleMigrator`.

---

## Databases Supported
- Postgres
- Cassandra
- Neo4j
- Mongo

---

## Usage

Migration files are executed in order, so a date-related naming convention is recommended, but not required. See the [example migrations folder](example/).

File extension names are ignored, so feel free to use whatever suits your fancy.

Downward migration scripts must end in `.down.<extension>`, but are optional.

``` bash
migrate postgres up|down \
    --connection-string | -c <postgres connection string> \
    --folder | -f <folder of migration files> \
    [--to <migration name to stop at>] \
    [--var <key=value pairs for variable replacement in scripts>]
```
``` bash
migrate cassandra up|down \
    --host | -h <cassandra hostname> \
    --port | -p <cassandra port> \
    --keyspace | -k <cassandra keyspace> \
    --folder | -f <folder of migration files> \
    [--to <migration name to stop at>]
```
``` bash
migrate neo4j up|down \
    --host | -h <neo4j hostname> \
    --port | -p <neo4j port> \
    --username | -u <neo4j username> \
    --password | -w <neo4j password> \
    --database | -d <neo4j database> \
    --folder | -f <folder of migration files> \
    [--to <migration name to stop at>]
```
``` bash
migrate mongo up|down \
    --connection-string | -c <mongo host connection string> \
    --database | -d <mongo database> \
    --folder | -f <folder of migration files> \
    [--to <migration name to stop at>]
```

### **Postgres**
The connection string must be an ADO.NET-compliant connection string.
Executes SQL commands from files.
All commands in a single file will be executed in a transaction.
The timeout for migration scripts is hardcoded to 10m.

### **Cassandra**
Executes CQL queries from files.
Only one command allowed per file.

*Be careful how you write your migrations since this applies to downward migrations as well.*

*Warning: does not yet support username/password*

### **Neo4j**
Executes Cypher queries from files.
Only one command allowed per file, though naturally queries that do multiple things are acceptable.

*Be careful how you write your migrations since this applies to downward migrations as well.*

### **Mongo**
Executes JSON commands from files.

---

## Contribution
While you have every right to fork this repo and run away with it, I would really appreciate issues, comments, and pull requests if you have them!
